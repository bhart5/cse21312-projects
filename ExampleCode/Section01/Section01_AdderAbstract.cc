/*****************************************
 * Filename: Section01_AdderAbstract.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file demonstrates the purpose of data abstract
 * through the creation of an Adder class that calls a 
 * method addNum that increments a private integer
 * of the Adder class 
 * **************************************/

#include <iostream>
using namespace std;

class Adder {
   public:
/*****************************************
 * Function Name: Adder
 * Pre-conditions: int i
 * Post-conditions: Creates object Adder
 * This is the constructor for the Adder object 
 ****************************************/
      Adder(int i = 0) {
         total = i;
      }
      
/*****************************************
 * Function Name: addNum
 * Pre-conditions: int
 * Post-conditions: none
 * Passes a value and increments it to the 
 * private number 
 ****************************************/
      void addNum(int number) {
         total += number;
      }
      
 /*****************************************
 * Function Name: getTotal
 * Pre-conditions: none
 * Post-conditions: int
 * Returns the value of the private total member
 ****************************************/
      int getTotal() {
         return total;
      };
      
   private:
      // hidden data from outside world
      int total;
};

/*****************************************
 * Function Name: main
 * Pre-conditions: int, char **
 * Post-conditions: int
 * This is the main driver function
 ****************************************/
int main(int argc, char **argv) {
   Adder a;
   
   a.addNum(10);
   a.addNum(20);
   a.addNum(30);

   cout << "Total " << a.getTotal() <<endl;
   return 0;
}
