/*******************************
 * File name: Section01_hanoi_stack.h 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the template class
 * for a stack for the Towers of Hanoi 
 * problem.
 * ****************************/

#ifndef SECTION_01_HANOI_STACK_H
#define SECTION_01_HANOI_STACK_H

#include <cstdlib>  // for size_t
#include <stack>

template <typename T>
class Tower{
    
    // Stacks
    std::stack<T> the_stack;
    
    public:
     
        // Counters for size to report
        // Don't need to unstack to get stack size.
        size_t data_size = 0;
        size_t tower_num = 0;
        
        /*********************************************
         * Function Name: Tower<T>
         * Preconditions: size_t, size_t 
         * Postconditions: none 
         * This is the main construct for a Tower stack 
         * ******************************************/        
        Tower<T>(size_t data_size, size_t tower_num) : data_size(data_size), tower_num(tower_num) {
            
        }

        /*********************************************
         * Function Name: getTowerNum
         * Preconditions: none 
         * Postconditions: size_t 
         * Return the stack number
         * ******************************************/  
        const size_t getTowerNum(){
            return tower_num;
        }
 
        /*********************************************
         * Function Name: empty
         * Preconditions: none 
         * Postconditions: bool 
         * This function returns a boolean indicating
         * if the main data stack is empty
         * ******************************************/
    	bool empty() const { 
    	    return the_stack.empty(); 
    	}

        /*********************************************
         * Function Name: top
         * Preconditions: none 
         * Postconditions: const T& 
         * This function returns the value of the top element
         * of the stack without popping the stack 
         * ******************************************/    	
        const T& top() const { 
            return the_stack.top(); 
        }

        /*********************************************
         * Function Name: push
         * Preconditions: const T&  
         * Postconditions: none 
         * This function pushes the value on the top 
         * of the data stack. If it is <= to the minimum,
         * or if >= to the max, then 
         * ******************************************/          
        void push(const T& value) {
            the_stack.push(value);   data_size++;
        }

        /*********************************************
         * Function Name: pop 
         * Preconditions: void
         * Postconditions: none 
         * This function pops the top value of the data 
         * stack, and if the value is equal to the max or 
         * min stack, it pops those values off as well.
         * ******************************************/           
        void pop() {
            the_stack.pop(); data_size--;
        }

};

#endif